#!/usr/bin/env bash

CONF_FILE=/boot/config.txt

if ! cat $CONF_FILE | grep "dtparam=act_led_trigger=none" &> /dev/null ; then
    if cat $CONF_FILE | grep "\[all\]" &> /dev/null ; then
        sed -i "s/^\[all\].*$/\0\n# Disable Activity LED\ndtparam=act_led_trigger=none\ndtparam=act_led_activelow=off/" $CONF_FILE
    else
        cat >> $CONF_FILE << EOF
[all]
# Disable Activity LED
dtparam=act_led_trigger=none
dtparam=act_led_activelow=off
EOF
    fi
fi

if ! cat $CONF_FILE | grep "dtparam=pwr_led_trigger=none" &> /dev/null ; then
    if cat $CONF_FILE | grep "\[all\]" &> /dev/null ; then
        sed -i "s/^\[all\].*$/\0\n# Disable Activity LED\ndtparam=pwr_led_trigger=none\ndtparam=pwr_led_activelow=off/" $CONF_FILE
    else
        cat >> $CONF_FILE << EOF
[all]
# Disable Activity LED
dtparam=pwr_led_trigger=none
dtparam=pwr_led_activelow=off
EOF
    fi
fi
